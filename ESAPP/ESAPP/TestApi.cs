﻿using ESBPM.Mobile.application.Cmd;
using ESBPM.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESAPP
{
    public class TestApi: BaseCmd
    {
        public TestApi(long UID, Dictionary<string, string> DICT)
            : base(UID,DICT)
        {
            
        }



        public override System.Dynamic.ExpandoObject BuildData()
        {
            log.Info("你好啊");
            if (urlDict.ContainsKey("userid"))
            {
                int userid = int.Parse(urlDict["userid"]);
                var userinfo = UserDept.GetUserById(userid);
                if (userinfo != null)
                {

                    Obj.Data =
                         new
                         {
                             id = userid,
                             Name = userinfo["Name"].ToString(),
                             Phone = userinfo["Mobile"].ToString(),
                             Email = userinfo["Email"].ToString(),
                             Job = userinfo["Job"].ToString(),
                             DeptName = userinfo["DeptName"].ToString(),
                         };
                    Msg.success = true;
                }
                else
                {
                    Msg.msg = "编号为[" + userid + "]的用户不存在！";
                }
            }
            else
            {
                Msg.msg = "值[userid]不能为空！";
            }

            return Obj;
        }
    }
}
