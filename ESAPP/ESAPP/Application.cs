﻿using CYQ.Data.Table;
using ESBPM.Mobile.application;
using ESBPM.Organization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ESAPP
{
   public class Application:IApps
    {
      

       public MDataTable GetSelfApps(int userid, MDataTable dtApps)
       {
           var U = UserDept.GetUserById(userid);
           string deptid = U["DeptID"].ToString();
           //只保留一个测试应用
           for (int i = 0; i < dtApps.Rows.Count; i++)
           {
               if (dtApps.Rows[i].Get<int>("id") != 28)
               {
                   dtApps.Rows.RemoveAt(i);
                   i--;
               }
           }
           return dtApps; ;
       }
    }
}
